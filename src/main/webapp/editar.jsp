<%@page import="com.emergentes.modelo.Tarea"%>
<%
    Tarea item = (Tarea) request.getAttribute("miTarea");
%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Document</title>
    </head>
    <body>  
        <h1><%=(item.getId() == 0) ? "Nueva tarea" : "Editar tarea"%></h1>

        <form action="" method="post">
            <input type="hidden" name="id" value="<%= item.getId()%>">
            <table border="0">
                <tr>
                    <td>Tarea</td>
                    <td><input type="text" name="tarea" value="<%= item.getTarea()%>"></td>
                </tr>

                <tr>
                    <td>Prioridad</td>
                    <td>
                        <select name="prioridad" id="prioridad">
                            <option value="Alta">Alta</option>
                            <option value="Media">Media</option>
                            <option value="Baja">Baja</option>
                        </select>
                    </td>
                </tr>

                <tr>
                    <td>Completado</td>
                    <td>
                        <select name="completado" id="">
                            <option value="Concluido">Concluido</option>
                            <option value="Pendiente">Pendiente</option>
                        </select>
                    </td>
                </tr>

                <tr>
                    <td></td>
                    <td><input type="submit" value="Enviar"></td>
                </tr>
            </table>
        </form>
    </body>
</html>
