<%@page import="java.util.ArrayList"%>
<%@page import="com.emergentes.modelo.Tarea"%>
<%
    ArrayList<Tarea> tarea = (ArrayList<Tarea>) session.getAttribute("listatar");
%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="UTF-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <title>Tareas</title>
    </head>
    <body>
        <h1>Lista de Tareas</h1>

        <a href="MainController?op=nuevo">Nuevo</a>

        <table border="1">
            <tr>
                <th>ID</th>
                <th>Tarea</th>
                <th>Prioridad</th>
                <th>Completado</th>
                <th></th>
                <th></th>
            </tr>
            <%
                if (tarea != null) {
                    for (Tarea item : tarea) {
            %>
            <tr>
                <td><%= item.getId()%></td>
                <td><%= item.getTarea()%></td>
                <td><%= item.getPrioridad()%></td>
                <td><input type="checkbox" disabled <%=item.getCompletado().equals("Concluido") ? "checked" : " "%>> </td>
                <td><a href="MainController?op=editar&id=<%= item.getId()%>">Editar</a></td>
                <td><a href="MainController?op=eliminar&id=<%= item.getId()%>"
                       " onclick="return confirm('Esta seguro de eliminar esta tarea')">Eliminar</a></td>
            </tr>
            <%
                    }
                }
            %>
        </table>
    </body>
</html>
